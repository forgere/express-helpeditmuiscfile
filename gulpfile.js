const gulp = require("gulp")
const {port} = require('./config.json')
const nodemon = require('nodemon');
const {series, watch} = gulp

gulp.task('serve', (cb) => {
    let start = false;
    nodemon({
      script: './bin/www',
    }).on('start', () => {
      if (!start) {
        cb();
        start = true;
      }
    });
})

gulp.task('default', series('serve')), () => {
    var files = [
        'views/**/*.pug',
        'routes/**/*.js',
        'public/**/*.*',
        'models/**/*.js'
    ]
    watch(files).on("change", browserSync.reload); 
}