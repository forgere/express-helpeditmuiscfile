var express = require('express');
var router = express.Router();
const url = require('url');
const music = require('../models/music')
const crawl = require('../models/crawl')
const {port} = require('../config.json')

function search(req, res, next){
  var queryData = url.parse(req.url, true).query;
  music.search(queryData)
    .then((data) => {
      res.render('index', {title: '搜索结果:' + queryData.sth, data, port: process.env.PORT || port || 3000})
    })
    .catch(err => {
      res.json(err)
    })
}

/* GET home page. */
router.get('/', search);

router.get('/add', function(req, res, next) {
  console.log(music.add())
})

router.get('/search', search)

router.post('/download', function(req, res, next) {
  music.download(JSON.parse(req.body.source))
    .then((data) => {
      res.json(data)
    })
    .catch(err => {
      res.json(err)
    })
})

router.post('/downLRC', function(req, res, next) {
  const source = JSON.parse(req.body.source)
  crawl.downLRC({id: source.id, source: source.source})
    .then(data => {
      res.json({
        data,
        mes: 'success',
        code: 200
      })
    })
    .catch(err => {
      res.json({
        err,
        code: 401,
        mes:'fail'
      })
    })
})

module.exports = router;
